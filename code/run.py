from flask import Flask, Response
import logging
import psycopg2
import redis
import sys

app = Flask(__name__)
cache = redis.StrictRedis(host='redis', port=6379)   
@app.route("/code-hello")
def hello0():

    return Response("Hi from your Flask app running in your Docker container!")

@app.route("/code")
def hello1():

    return Response("Hi from your Flask_app running in your Docker container!")

#@app.route("/")
#def hello2():

#    return Response("Hi from your Flask app running in your Docker container!")

if __name__ == "__main__":
    app.run("0.0.0.0", port=5000, debug=True)
